import gql from "graphql-tag";
import { InMemoryCache } from 'apollo-cache-inmemory';
import ApolloClient from "apollo-boost";
import { baseUrl } from "../../../../../constants";

const cache = new InMemoryCache();

const client = new ApolloClient({
    cache,
    uri: baseUrl.server,
});

// Get all ticket templates under tenant
const GET_TICKET_TEMPLATES = gql`
    query GetTicketTemplate($tenantId:Int!){
        ticketTemplate(tenantId:$tenantId){
            id,templateName,
            ticketDescription,
            ticketPrefix,tenantId
        }
    }
`;

async function getTicketTemplatesData(data) {
    let result;
    await client
        .query({
            query: GET_TICKET_TEMPLATES, variables: { tenantId: data.tenantId }
        }).then(res => {
            result = res.data.ticketTemplate;
        });
    return result;
};

// Get Useremails
const GET_USER_EMAILS = gql`
   {
        getAllUsers{
            id,firstName,emailIs
        }
    }
`;

async function getUserEmails(data) {
    let result;
    await client
        .query({
            query: GET_USER_EMAILS
        }).then(res => {
            result = res.data.getAllUsers;
        });
    return result;
};


// Get all priority
const GET_PRIORITY = gql`
    {
        priorities{
            id,
            priorityname
        }
    }
`;
async function getAllPriority() {
    let result;
    await client.query({
        query: GET_PRIORITY
    }).then(res => {
        result = res.data.priorities;
    })
    return result;
}

// Get all ticket types
const GET_TICKET_TYPES = gql`
    {
        getAllTicketType{
            id,
            ticketType
        }
    }
`;
async function getAllTicketTypes() {
    let result;
    await client.query({
        query: GET_TICKET_TYPES
    }).then(res => {
        result = res.data.getAllTicketType;
    })
    return result;
}

// Get all Tags
const GET_TAGS = gql`
    query 
    GetAllTags($companyId:Int){
        allTags(
            companyId:$companyId
        ){
            id,
            tagTitle
        }
    }
`;
async function getAllTags() {
    let result;
    await client.query({
        query: GET_TAGS, variables: { companyId: 1 }
    }).then(res => {
        result = res.data.allTags;
    })
    return result;
}
export {
    getTicketTemplatesData,
    getUserEmails,
    getAllPriority,
    getAllTicketTypes,
    getAllTags
}

