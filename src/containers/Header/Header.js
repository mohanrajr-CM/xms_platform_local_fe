import React from 'react';
import './Header.scss'

import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select'
import Button from '@material-ui/core/Button';

import searchIcon from '../../assets/Sidenavbar-icons/Icon feather-search-small.svg'
import alertIcon from '../../assets/icons/SVG/Iconfeather-bell.svg'
import inActiveAddIcon from '../../assets/Sidenavbar-icons/Group 11408.svg'
import nineDotsIcon from '../../assets/Sidenavbar-icons/9dots.svg'
import profileImage from '../../assets/images/profile.png'
import Tooltip from '@material-ui/core/Tooltip';
import { makeStyles } from '@material-ui/core/styles';


const headerIcons = [
  {id: 1, icon: alertIcon, tooltip:"Notification"},
  {id: 2, icon: inActiveAddIcon, tooltip:"Create"},
  {id: 3, icon: nineDotsIcon, tooltip:"XMS Apps"},

]

function arrowGenerator(color) {
  return {
    '&[x-placement*="bottom"] $arrow': {
      top: 0,
      left: 0,
      marginTop: '-0.95em',
      width: '5em',
      height: '0.5em',
      '&::before': {
        borderWidth: '0 1em 1em 1em',
        borderColor: `transparent transparent #ffffff transparent`,
      },
    },
    '&[x-placement*="top"] $arrow': {
      bottom: 0,
      left: 0,
      marginBottom: '-0.95em',
      width: '2em',
      height: '1em',
      '&::before': {
        borderWidth: '1em 1em 0 1em',
        borderColor: `${color} transparent transparent transparent`,
      },
    },
    '&[x-placement*="right"] $arrow': {
      left: -2,
      marginLeft: '-0.95em',
      height: '2em',
      width: '2em',
      '&::before': {
        borderWidth: '1em 1em 1em 0',
        borderColor: `transparent #ffffff transparent transparent`,
      },
    },
    '&[x-placement*="left"] $arrow': {
      right: 0,
      marginRight: '-0.95em',
      height: '2em',
      width: '1em',
      '&::before': {
        borderWidth: '1em 0 1em 1em',
        borderColor: `transparent transparent transparent ${color}`,
      },
    },
  };
}


const useStylesArrow = makeStyles(theme => ({
  tooltip: {
    position: 'relative',
    fontSize: 10, 
    borderRadius: 100,
    backgroundColor: "#ffffff",
    color: "#656565",
    boxShadow: "0 3px 12px 0 #d2d2d2",
    marginLeft: '0.1em',
    padding: "4px 12px 4px 12px",
    textAlign: 'center'
  },
  arrow: {
    position: 'absolute',
    fontSize: 6,
    '&::before': {
      content: '""',
      margin: 'auto',
      display: 'block',
      width: 0,
      height: 0,
      borderStyle: 'solid',
    },
  },
  popper: arrowGenerator(theme.palette.grey[700]),
}));

function ArrowTooltip(props) {
  const { arrow, ...classes } = useStylesArrow();
  const [arrowRef, setArrowRef] = React.useState(null);

  return (
    <Tooltip 
      classes={classes}
      PopperProps={{
        popperOptions: {
          modifiers: {
            arrow: {
              enabled: Boolean(arrowRef),
              element: arrowRef,
            },
          },
        },
      }}
      {...props}
      title={
        <React.Fragment>
          {props.title}
          <span className={arrow} ref={setArrowRef} />
        </React.Fragment>
      }
    />
  );
}


class HeaderSection extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      appName: 'xcelpros'
    }
  }

  handleChange =event=> {
    this.setState({ [event.target.name]: event.target.value })
  }
  logoutHandler = async (event) => {
    localStorage.clear();
    window.location.href = '/';
  }

  render() {
    return (
      <div className='header-container'>
        <div className='header-search-section'>
          <div className="header-srch-br">
            <img className='header-search-icon' src={searchIcon} alt='searchIcon'/>
            <TextField className={''} placeholder='Search anything...' margin="normal" />
          </div>
          
        </div>

        <div className='header-right-section'>
          <FormControl variant="outlined" className={'application-type-dropdown'}>
            <Select
              value={this.state.appName}
              onChange={this.handleChange}
            >
              <MenuItem value={'xcelpros'}>XCELPROS</MenuItem>
              <MenuItem value={20}>Demo name</MenuItem>
              <MenuItem value={30}>Demo name</MenuItem>
            </Select>
          </FormControl>      
          <Button className='logout-button' onClick={this.logoutHandler} >Logout</Button>    
          <div className='header-icons-section'>
            { headerIcons.map(icon => 
                <ArrowTooltip title={icon.tooltip} placement="bottom">
                  <img src={icon.icon} alt={icon.icon}/>
                </ArrowTooltip>)}
          </div>
          <div className='header-profile-image'>
            <img src={profileImage} alt='profileImage'/>
          </div>
        </div>

      </div>
    )
  }
}
export default HeaderSection
